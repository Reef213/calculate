package com.calculate.reef.calculate;


import android.util.Log;
import android.view.View;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by reef on 03.04.15.
 */
public class Calculate{


    private final static String TAG="MyApp";
    public Stack<String> m_stk = new Stack<String>();
    public static Stack<String> journal_stk = new Stack<String>();
    public String m_display = "";
    public String textDisplay= "";
    public String[] value={"Tomy_cat","Marry_cat","Napoleon_cat","Boris_cat","Hello_catty"};




    public Calculate() {
        m_stk.push("0");
    }
    /**
    *метод calculate() производит математические операции (+,-,*,/)
    *на переменными dOperand1 и dOperand2 и записывает результат в m_display
    */

    public void calculate() {
        Double dOperand2 = Double.parseDouble(m_stk.pop());
        String strOperation = m_stk.pop();
        Double dOperand1 = Double.parseDouble(m_stk.pop());
        Double dResult = 0.0;

        if ("+".equals(strOperation)) {
            dResult = dOperand1 + dOperand2;
        }
        if ("-".equals(strOperation)) {
            dResult = dOperand1 - dOperand2;
        }
        if ("/".equals(strOperation)) {
            dResult = dOperand1 / dOperand2;
        }
        if ("*".equals(strOperation)) {
            dResult = dOperand1 * dOperand2;
        }

        if (0.0 != dResult) {
            m_display=cheking_subString(dResult.toString());
            setJournal_stek(m_display);
        } else {
            m_display = "";
        }
//        m_display = dResult.toString();
    }



    private void setJournal_stek(String str)
    {
     journal_stk.push(str);
    }

    private String cheking_subString(String str)
    {
     String return_string="";

     String[] sub_string=str.split("\\.");

     if("0".equals(sub_string[1]))
     {
      return_string=sub_string[0];
     }else return_string=str;
        return return_string;
    }

    private void cheking_dot(String str,Pattern dot_p)
    {
        if(!m_display.isEmpty()){
        Matcher regex_dot=dot_p.matcher(m_display);
        if(!regex_dot.find())
        {
            m_display+=str;
        }}else
        {
            m_display+="0.";
        }
    }

    private void cheking_zero(String str) {
        if(m_display.equals("0"))
        {
            if(!str.equals("0"))
            {
                m_display=str;
            }
        }else
        {
          m_display+=str;
        }
    }

    public void buttonClicked(String str) {
        Pattern number_p = Pattern.compile("[0-9]");
        Pattern dot_p = Pattern.compile("[.]");


        if ("CE".equals(str)) {
            m_stk.clear();
            m_display = "";
            return;
        }

        if ("C".equals(str)) {
            if(!"".equals(m_display)){
            int length=m_display.length();
            String temp_string=m_display.substring(0,length-1);
            m_display = temp_string;
            return;}
        }


        if (number_p.matcher(str).matches()) {
            cheking_zero(str);
        }
        else
        if (".".equals(str)){

            cheking_dot(str,dot_p);

        } else {
            if (m_stk.size() >= 2) {
                m_stk.push(textDisplay);
                calculate();
                m_stk.clear();
                m_stk.push(textDisplay);
                if (str != "=") {
                    m_stk.push(str);
                }
            } else {
                m_stk.push(textDisplay);
                m_stk.push(str);
                m_display = "";
            }
        }


    }


    public String getTextDisplay(String str)
    {
        textDisplay=str;
        return textDisplay;
    }
}