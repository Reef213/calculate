package com.calculate.reef.calculate;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


public class ListOfOperation extends Activity {
    Calculate c = new Calculate();
    ArrayList<String> mList = new ArrayList<String>();
    private String value = "0";


    //Create Journal List
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addListView();
        clickItemListView();

    }

    //add ListView to Activity
    private void addListView() {

        mList.add(c.value.toString());
        ArrayList<String> listS;
        listS = (ArrayList<String>) getIntent().getExtras().get("journal_values");
        setContentView(R.layout.activity_list_of_operation);
        ArrayAdapter<String> myArrayAdapter =
                new ArrayAdapter<>(this, R.layout.list_item, listS);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(myArrayAdapter);

    }
    //void called for click Item to ListView
    private void clickItemListView() {

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                TextView textView = (TextView) viewClicked;
                value = textView.getText().toString();
            }
        });
    }


    //Click Button Cancel
    public void onBackCancel(View view) {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    //Click Button Ok
    public void onBackOk(View view) {
        Intent intent = new Intent();
        intent.putExtra("display_value", value);
        setResult(RESULT_OK, intent);
        finish();
    }
}

