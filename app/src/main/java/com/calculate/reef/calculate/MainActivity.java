package com.calculate.reef.calculate;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {

    public final static String TAG = "MyApp";
    static final private int CONST_JOURNAL = 0;
    private final static String KEY_INDEX = "index";
    private Calculate calculate = new Calculate();
    private boolean flag_for_journal = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            calculate.m_display = savedInstanceState.getString(KEY_INDEX, "");
        }

        updateQuestion();
    }


    private void updateQuestion() {
        final TextView display = (TextView) findViewById(R.id.display);
        if (!"".equals(calculate.m_display)) display.setText(calculate.m_display);
        else {
            display.setText("0");
//            calculate.m_display="0";
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_INDEX, calculate.m_display);
    }


    //Click button to aplication (1,2,3..)
    public void onClick(View view) {
        String textToButton;

        final Button but = (Button) findViewById(view.getId());
        final TextView display = (TextView) findViewById(R.id.display);
        textToButton = (String) but.getText();
        calculate.getTextDisplay(getTextDisplay());
        calculate.buttonClicked(textToButton);

        if (!"".equals(calculate.m_display)) {
            display.setText(calculate.m_display);
        } else {
            display.setText("0");
        }
    }

    public void onOpenJournal(View view) {
        Intent intent = new Intent(getApplicationContext(), ListOfOperation.class);

        intent.putExtra("journal_values", Calculate.journal_stk);
        super.startActivityForResult(intent, CONST_JOURNAL);
        flag_for_journal = true;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CONST_JOURNAL) {
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "d_v=" + data.getExtras().getString("display_value"));
                calculate.m_display = data.getStringExtra("display_value");
                updateQuestion();
            }
        }

    }

    public String getTextDisplay() {
        String s;
        final TextView display = (TextView) findViewById(R.id.display);
        s = display.getText().toString();
        return s;
    }


}
